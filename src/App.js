import React from 'react'
import AppNavigator from './navigation/AppNavigator'
import { NavigationContainer, DarkTheme } from '@react-navigation/native';
import * as eva from '@eva-design/eva';
import { ApplicationProvider } from '@ui-kitten/components';
import AuthContextProvider from './contexts/AuthContext';

export default function App() {
  return (
    <ApplicationProvider {...eva} theme={eva.dark}>
      <NavigationContainer theme={DarkTheme}>
        <AuthContextProvider>
          <AppNavigator />
        </AuthContextProvider>
      </NavigationContainer>
    </ApplicationProvider>
  )
}