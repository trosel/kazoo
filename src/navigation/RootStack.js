import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import ChatListScreen from "../scenes/chatList/ChatListScreen";

const Stack = createStackNavigator();

export default function RootStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="ChatList"
        component={ChatListScreen}
        options={{ title: 'Chats' }}
      />
    </Stack.Navigator>
  );
}
