import React, { Component, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { TouchableWithoutFeedback, StyleSheet, View } from 'react-native';
// import Identicon from './Identicon'
import { util, Chat } from 'iris-lib'
import gunService from '../../../services/GunService';
import { Text } from '@ui-kitten/components';
// import Svg from 'App/Components/Svg'
// import { SvgXml } from 'react-native-svg'

export default function ChatListItem({chat}) {
  const [isTyping, setIsTyping] = useState(false)
  const [lastSeenTime, setLastSeenTime] = useState(null)
  const [isOnline, setIsOnline] = useState(null)

  const navToChatScreen = () => {
    // () => NavigationService.navigate('ChatScreen', { pub: chat.pub, title: chat.name })
  }

  const renderLatestSeen = () => {
		if (chat.latest && chat.latest.info.selfAuthored && !isTyping) {
			const t = chat.latest.date.toISOString()
			// if (lastSeenTime >= t) {
			// 	return (<View style={ApplicationStyles.listItem.checkmark}><SvgXml xml={Svg.seenCheckmark} width={15} height={15} /></View>)
			// } else if (isOnline && isOnline.lastActive >= t) {
			// 	return (<View style={ApplicationStyles.listItem.checkmark}><SvgXml xml={Svg.deliveredCheckmark} width={15} height={15} /></View>)
			// } else {
			// 	return (<View style={ApplicationStyles.listItem.checkmark}><SvgXml xml={Svg.sentCheckmark} width={15} height={15} /></View>)
			// }
		} else return (<Text></Text>)
  }
  
  const initState = () => {
		chat.getTheirMsgsLastSeenTime(setLastSeenTime)
		Chat.getOnline(gunService, chat.pub, setIsOnline)
		chat.getTyping(setIsTyping)
  }

  useEffect(() => {
    initState()
  }, [])

  let latestTimeText = ''
  if (chat.latest) {
    latestTimeText = util.getDaySeparatorText(chat.latest.date, chat.latest.date.toLocaleDateString({dateStyle:'short'}))
    if (latestTimeText === 'today') { latestTimeText = util.formatTime(chat.latest.date) }
  }

  return (
    <TouchableWithoutFeedback onPress={navToChatScreen}>
     <View style={styles.listItem.item}>
       {/* <Identicon pub={chat.pub} style={styles.listItem.identicon} /> */}
       <View style={styles.listItem.text}>
          <View style={styles.listItem.nameRow}>
           <Text style={styles.listItem.name}>{chat.name || ''}</Text>
           <Text style={styles.listItem.message}>{latestTimeText}</Text>
         </View>
         <View style={styles.listItem.messageRow}>
            {renderLatestSeen()}
            <Text style={isTyping ? {...styles.listItem.typing, flex: 1} : {...styles.listItem.message, flex: 1}}>{(isTyping && 'Typing...') || (chat.latest && chat.latest.text) || ''}</Text><Text style={styles.listItem.typing}>{chat.hasUnseen ? '*' : ''}</Text>
         </View>
       </View>
     </View>
    </TouchableWithoutFeedback>
  );
}

const styles = {
  screen: {
    container: {
      flex: 1,
    },
  },
  identicon: {
    overflow: 'hidden',
    maxWidth: '100%',
  },
  listItem: {
    checkmark: {
      marginRight: 4
    },
    item: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    identicon: {
      marginHorizontal: 8,
    },
    text: {
      flex: 1,
      paddingTop: 16,
      paddingBottom: 16,
      paddingLeft: 8,
      paddingRight: 8,
      borderBottomWidth: 1,
      borderColor: '#eee',
    },
    name: {
      textAlign: 'left',
      fontWeight: 'bold',
      flex: 1,
    },
    messageRow: {
      flexDirection: 'row',
    },
    nameRow: {
      flexDirection: 'row',
    },
    message: {
      color: '#777',
      textAlign: 'left',
      marginBottom: 5,
    },
    typing: {
      color: '#05b246',
      fontWeight: 'bold',
      textAlign: 'left',
      marginBottom: 5,
    },
  },
}