import React, { useState } from 'react';
import { View, StyleSheet } from 'react-native';
import { Text, Input, Button } from '@ui-kitten/components';

export default function SignupForm({logInAsNewUser}) {
  const [name, setName] = useState('')

  return (
    <>
      <Input
          size='large'
          style={styles.radius}
          autoCapitalize="words"
          autoCorrect={false}
          keyboardType="default"
          editable
          maxLength={40}
          autoFocus
          placeholder="What's your name?"
          placeholderTextColor="white"
          selectionColor="white"
          value={name}
          onChangeText={setName}
        />
        <Button style={[styles.radius, {width: '100%', marginTop: 12}]} onPress={logInAsNewUser}>Go!</Button>
    </>
  )
}


const styles = StyleSheet.create({
  radius: {
    borderRadius: 12,
  }
})